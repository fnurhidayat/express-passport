module.exports = {

  successResponse: (data, meta) => ({
    status: 'success',
    meta,
    data,
  }),

  errorResponse: errors => ({
    status: 'fail',
    errors
  })

}
