const createRouter = require('express').Router
const session = require('express-session')
const passport = require('./lib/passport')

// Controllers
const admin = require('./controllers/admin')
const api = require('./controllers/api')

// Middlewares
const restrict = require('./middlewares/restrict')

// Routers
const adminRouter = createRouter() 
const apiRouter = createRouter()
const router = createRouter()

// === Admin Router -> /admin
/* Set the session handler on adminRouter 
 * So the session will only work on this router
 * And it is handy, since we don't have to run
 * This middleware on the API only Endpoint
 * */
adminRouter.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false
  })
)
/* Initialize Passport
 * And tell passport to use session
 * */
adminRouter.use(passport.initialize())
adminRouter.use(passport.session())
adminRouter.get('/', admin.pages.default['index'])
adminRouter.get('/login', admin.pages.auth['login'])
/* Passport with automatically handle the Login Request
 * By calling the passport instance that you've created
 * With authenticate method
 *
 * If the request is valid,
 * then it will redirect to the admin homepage
 * otherwise it will return to the login page
 * */
adminRouter.post(
  '/api/login',
  passport.authenticate('local', {
    successRedirect: '/admin',
    failureRedirect: '/admin/login',
    failureFlash: true
  })
)
adminRouter.get('/api/logout', admin.pages.auth.logout)
adminRouter.get('/dashboard/products', restrict, admin.pages.dashboard.products.index)
adminRouter.use(admin.pages.default['404']) // 404 Handler for Admin
router.use('/admin', adminRouter)

// === Api Router -> /api
apiRouter.post('/auth/register', api.auth.register)
apiRouter.post('/auth/login', api.auth.login)
router.use('/api', apiRouter)

module.exports = router
