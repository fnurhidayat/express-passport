'use strict'

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { SECRET_KEY = 'Rahasia' } = process.env

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: {
          args: true,
          msg: 'Invalid email'
        }
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    underscored: true,
  })

  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        name: this.name,
        email: this.email,
      }
    }
  })

  // "Private" static _encrypt
  User._encrypt = password => bcrypt.hashSync(password, 10)

  // Public static register
  User.register = async function({ name, email, password }) {
    try {
      const instance = await this.create({ name, email, encrypted_password: this._encrypt(password) })
      return Promise.resolve(instance)
    }

    catch(err) {
      return Promise.reject(err)
    }
  }
 
  User.authenticate = async function({ email, password }) {
    try {
      const instance = await this.findOne({
        where: { email }
      })
      if (!instance)
        return Promise.reject(
          new Error('Email hasn\'t registered')
        )
      if (!(await instance.isPasswordValid(password)))
        return Promise.reject(
          new Error('Wrong password')  
        )
      return Promise.resolve(instance)
    }

    catch(error) {
      return Promise.reject(error)
    }
  }


  User.prototype.isPasswordValid = function (password) {
    return bcrypt.compare(password, this.encrypted_password)
  }

  User.prototype.generateAccessToken = function() {
    return jwt.sign(this.entity, SECRET_KEY)
  }

  return User
}
