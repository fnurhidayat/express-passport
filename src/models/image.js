'use strict'
module.exports = (sequelize, DataTypes) => {
  const Image = sequelize.define('Image', {
    alt: { 
      type: DataTypes.TEXT,
      allowNull: true
    },
    url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
  }, {
    underscored: true,
  })

  return Image
}
