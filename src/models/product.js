'use strict'
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    price: DataTypes.FLOAT,
    stock: DataTypes.INTEGER,
    image_id: DataTypes.INTEGER
  }, {
    underscored: true,
  })
  Product.associate = function(models) {
    Product.hasOne(models.Image)
  }
  return Product
}
