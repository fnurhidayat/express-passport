const { IMAGEKIT_PRIVATE_KEY, IMAGEKIT_PUBLIC_KEY, IMAGEKIT_URL_ENDPOINT } = process.env
const imagekit = new require('imagekit')({
  publicKey: IMAGEKIT_PUBLIC_KEY, 
  privateKey: IMAGEKIT_PRIVATE_KEY,
  urlEndpoint: IMAGEKIT_URL_ENDPOINT
})

module.exports = imagekit
