const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { User } = require('../models')

/* Function that specify how you authenticate the user
 * This is actually will work on local strategy,
 * There's no guarantee that it will also work on another strategy
 * */
async function localAuth(email, password, done) {
  try { 
    const user = await User.authenticate({ email, password })
    return done(null, user)
  }

  catch(err) {
    return done(null, false, { message: err.message }) 
  }
}

/* Tell passport how to handle the local strategy
 * You can assign more strategy on Passport
 * For example, if you want to add Google Login
 * You can just use more strategy for that
 * */
passport.use(
  new LocalStrategy({
    usernameField: 'email',
  }, localAuth)
)

/* Serialize and Deserialize
 * It means, how would you store and remove the object
 * From the session
 * */
passport.serializeUser(
  (user, done) => done(null, user.id)
)
passport.deserializeUser(
  async (id, done) => done(null, await User.findByPk(id))
)

module.exports = passport
