const express = require('express')
const flash = require('express-flash')
const path = require('path')
/*
 * Express configuration
 * - Morgan (Request Logger)
 * - Pug (View Engine) 
 * - Passport
 * */
const app = express()
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(flash())
app.set('views', path.resolve(__dirname, 'views'))
app.set('view engine', 'pug')
if (process.env.NODE_ENV !== 'production')
  require('dotenv').config()
if (process.env.NODE_ENV !== 'test')
  app.use(require('morgan')('dev'))
const router = require('./router')
app.use(router)
app.use(express.static('public'))

module.exports = app
