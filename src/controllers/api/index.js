module.exports = {
  auth: require('./authController'),
  products: require('./productController')
}
