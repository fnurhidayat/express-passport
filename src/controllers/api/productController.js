const { Image, Product } = require('../../models')
const { successResponse, errorResponse } = require('../../helpers/responseFormatter')
const faker = require('faker')

module.exports = {

  create: async (req, res) => {
    const { name, description, price, stock } = req.body

    try {
      const image = await Image.create({
        alt: name,
        url: faker.image.fashion() 
      })
      const product = await Product.create({
        name,
        description,
        image_id: image.id,
        price,
        stock,
      })

      res.status(201).json(
        successResponse({ product })
      )
    }

    catch(err) {
      res.status(422).json(
        errorResponse(err.message)
      )
    }
  }

}
