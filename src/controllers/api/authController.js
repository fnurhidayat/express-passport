const { User } = require('../../models')
const {
  successResponse,
  errorResponse
} = require('../../helpers/responseFormatter')

module.exports = {

  register: async (req, res) => {
    try {
      const user = await User.register(req.body) 
      res.status(201).json(
        successResponse({ user: user.entity })
      )
    }

    catch(err) {
      res.status(422).json(
        errorResponse(err.message)
      )
    }
  },

  login: async (req, res) => {
    try {
      const user = await User.authenticate(req.body)
      res.status(200).json(
        successResponse({
          user: {...user.entity, access_token: user.generateAccessToken()}
        })
      )
    }

    catch(err) {
      res.status(401).json(
        errorResponse(err.message)
      )
    }
  }

}
