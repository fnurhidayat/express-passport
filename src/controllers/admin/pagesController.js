module.exports = {
  default: {
    'index': (req, res) => res.status(200).render('admin/pages/default/index', { user: req.user }),
    '404': (req, res) => res.status(404).render('admin/pages/default/404'),
  },
  auth: {
    'login': (req, res) => {
      if (req.isAuthenticated())
        return res.redirect('/admin')
      res.render('admin/pages/auth/login')
    },
    'logout': (req, res) => {
      req.logOut()
      res.redirect('/admin/login')
    },
  },
  dashboard: {
    products: {
      index: (req, res) => res.render('admin/pages/dashboard/products/index', { user: req.user }),
      create: (req, res) => res.render('admin/pages/dashboard/products/create')
    }
  }
}
