const { Image, Product } = require('../../models')
const faker = require('faker')

module.exports = {
  create: async (req, res) => {
    const { name, description, price, stock } = req.body

    try {
      const image = await Image.create({
        alt: name,
        url: faker.image.fashion() 
      })
      const product = await Product.create({
        name,
        description,
        image_id: image.id,
        price,
        stock,
      })

      req.flash('info', `${product.name} is successfully added with ID ${product.id}`)
      res.status(201).redirect('/admin/dashboard/products')
    }

    catch(err) {
      req.flash('error', `${err.message}`)
      res.status(422).redirect('/admin/dashboard/products')
    }
  
  }
}
