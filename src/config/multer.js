const multer = require('multer')
const path = require('path')

module.exports = {
  development: multer.diskStorage({
    destination: (req, file, cb) => cb(null, path.resolve(__dirname, '..', '..', 'public', 'uploads')),
    filename: (req, file, cb) => {
      const split = file.originalname.split('.')
      const extention = split[split.length - 1]
      const name = `${file.fieldname.toUpperCase()}-${Date.now()}.${extention}` 
      req.file.url = `/uploads/${name}`
      cb(null, name)
    }
  }),
}
