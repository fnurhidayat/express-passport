const multer = require('multer')
const config = require('../config/wmulter')
const env = process.env.NODE_ENV || 'development'

module.exports = multer(config[env])
